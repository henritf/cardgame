package no.ntnu.idatx2001.oblig3.cardgame;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * The GUI for the card game.
 * This class creates the application and handles the GUI.
 * It has buttons to deal with all functionality of the program.
 *
 * @author Henrik Tobias Fredriksen
 * @version 2023-03-20
 */
public class CardGameApp extends Application {

    private DeckOfCards deck;
    private HandOfCards hand;
    private ArrayList<PlayingCard> dealtCards;

    private StackPane handBox = new StackPane();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws NullPointerException {
        Font.loadFont(getClass().getResourceAsStream("/Font/font.ttf"), 14);
        deck = new DeckOfCards();
        deck.shuffle();

        hand = new HandOfCards();


        //contains the entire layout of the application.
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);

        //contains the cards in the hand
        handBox.setStyle("-fx-padding: 10");
        root.getChildren().add(handBox);

        //Button deals cards
        Button dealButton = new Button("Deal Cards");
        dealButton.setOnAction(e -> {
            handBox.getChildren().clear();
            hand.getHand().clear();
            System.out.println("Dealing cards");

            //Creating a colored box around the cards
            Region backdrop = new Region();
            backdrop.setMinSize(680,206);
            backdrop.setMaxSize(680,206);
            backdrop.setStyle("-fx-background-color: #000000; -fx-border-color: #c51515; -fx-border-width: 7px;");

            HBox cardLabels = new HBox(5);
            cardLabels.setAlignment(Pos.CENTER);

            dealtCards = deck.dealHand(5);
            for (PlayingCard card : dealtCards) {
                hand.addCard(card);
                String imagePath = card.getImagePath();
                InputStream imageStream = getClass().getResourceAsStream(imagePath);
                System.out.println("Image path: " + imagePath + " ImageStream: " + imageStream);
                Image cardImage = new Image(imageStream);
                ImageView cardImageView = new ImageView(cardImage);
                cardImageView.setFitWidth(128);
                cardImageView.setPreserveRatio(true);
                cardImageView.setSmooth(true);
                cardLabels.getChildren().add(cardImageView);
            }
            handBox.getChildren().addAll(backdrop, cardLabels);
        });

        //Button shuffles the deck
        Button shuffleButton = new Button("Restock/Shuffle Deck");
        shuffleButton.setOnAction(e -> {
            deck = new DeckOfCards();
            deck.shuffle();
            System.out.println("New deck added and shuffled");
        });

        /**
          Button addFlushOnHand = new Button("Add Flush");
        addFlushOnHand.setOnAction(e -> {
            handBox.getChildren().clear();
            hand.getHand().clear();
            System.out.println("Dealing cards");

            //Creating a colored box around the cards
            Region backdrop = new Region();
            backdrop.setMinSize(680,206);
            backdrop.setMaxSize(680,206);
            backdrop.setStyle("-fx-background-color: #000000; -fx-border-color: #c51515; -fx-border-width: 7px;");


            HBox cardLabels = new HBox(5);
            cardLabels.setAlignment(Pos.CENTER);

            dealtCards = deck.dealFlush(5);
            for (PlayingCard card : dealtCards) {
                String imagePath = card.getImagePath();
                InputStream imageStream = getClass().getResourceAsStream(imagePath);
                System.out.println("Image path: " + imagePath + " ImageStream: " + imageStream);
                Image cardImage = new Image(imageStream);
                ImageView cardImageView = new ImageView(cardImage);
                cardImageView.setFitWidth(128);
                cardImageView.setPreserveRatio(true);
                cardImageView.setSmooth(true);
                cardLabels.getChildren().add(cardImageView);
            }
            handBox.getChildren().addAll(backdrop, cardLabels);
        });
            */

        //contains the buttons
        HBox buttonBox = new HBox(10);
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.getChildren().addAll(dealButton, shuffleButton);
        root.getChildren().add(buttonBox);

        //contains the sum of the hand
        Button sumButton = new Button("Sum of hand");
        Label sumLabel = new Label();
        sumButton.setOnAction(e -> {
            int sum = hand.calculateHandValue();
            sumLabel.setText("Sum of hand: " + sum);
        });

        //contains the information about the hand
        Button heartAceButton = new Button("Check for Heart Ace");
        Label heartAceLabel = new Label();
        heartAceButton.setOnAction(e -> {
            boolean hasHeartAce = hand.hasHeartAce();
            if (hasHeartAce) {
                heartAceLabel.setText("Hand contains Heart Ace");
            } else {
                heartAceLabel.setText("Hand does not contain Heart Ace");
            }
        });


        Button flushButton = new Button("Check for Flush");
        Label flushLabel = new Label();
        flushButton.setOnAction(e -> {

            boolean hasFlush = hand.isFiveFlush();
            flushLabel.setText(hasFlush ? "Flush: Yes" : "Flush: No");

        });

        Button heartsButton = new Button("Show Hearts");
        Label heartsLabel = new Label();
        heartsButton.setOnAction(e -> {
            String Hearts = hand.getHeartsAsString();
            heartsLabel.setText(Hearts);
        });

        VBox infoBox = new VBox(10);
        infoBox.setAlignment(Pos.BASELINE_LEFT);

        infoBox.getChildren().addAll(sumButton, sumLabel, heartAceButton, heartAceLabel, flushButton, flushLabel, heartsButton, heartsLabel);
        root.getChildren().add(infoBox);

        Scene scene = new Scene(root,1280,720);
        //link the CSS file to the scene
        scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());
        primaryStage.setScene(scene);
        primaryStage.setTitle("Card Game");
        primaryStage.show();

        Image appIcon = new Image(getClass().getResourceAsStream("/icon.png"));
        primaryStage.getIcons().add(appIcon);

    }
}
