package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Handles the cards on hand of a player,
 * can check it for contents and value.
 *
 * @author Henrik Tobias Fredriksen
 * @version 2023-03-20
 */
public class HandOfCards {

    private ArrayList<PlayingCard> hand;

    /**
     * Instantiates a new Hand of cards.
     */
    public HandOfCards() {
        hand = new ArrayList<>();
    }

    /**
     * Gets hand of cards.
     *
     * @return the hand
     */
    public ArrayList<PlayingCard> getHand() {
        return hand;
    }


    /**
     * Adds a card to the hand.
     *
     * @param card the hand of cards
     */
    public void addCard(PlayingCard card) {
        hand.add(card);
    }
    /**
     * removes a card from hand
     * @param i the index of the card to be removed
     */
    public void removeCard(int i) {
        hand.remove(i);
    }

    /**
     * Gets hand size.
     *
     * @return the hand size
     */
    public int getHandSize() {
        return hand.size();
    }

    /**
     * Calculates the value of the hand.
     *
     * @return the hand value of all card ranks 1-13 on hand.
     */
    public int calculateHandValue() {
        int handValue = 0;
        for (PlayingCard card : hand) {
            handValue += card.getFace();
        }
        return handValue;
    }

    /**
     * Checks if the hand has an ace.
     *
     * @return true if the hand has an ace, false if not.
     */
    public boolean hasHeartAce() {
        for (PlayingCard card : hand) {
            if (card.getFace() == 1 && card.getSuit() == 'H') {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if the hand has a five flush.
     *
     * @return true if the hand has a five flush, false if not.
     */
    public boolean isFiveFlush() {
        int[] suitCounts = new int[4];

        for (PlayingCard card : hand) {
            int suitIndex = switch (card.getSuit()) {
                case 'S' -> 0;
                case 'H' -> 1;
                case 'D' -> 2;
                case 'C' -> 3;
                default -> throw new IllegalStateException("Unexpected value: " + card.getSuit());
            };
            suitCounts[suitIndex]++;
        }

        for (int count : suitCounts) {
            if (count == 5) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets hearts on the hand as string.
     *
     * @return the hearts as string
     */
    public String getHeartsAsString() {
        ArrayList<PlayingCard> hearts = new ArrayList<>();
        for (PlayingCard card : hand) {
            if (card.getSuit() == 'H') {
                hearts.add(card);
            }
        }
        if(hearts.isEmpty()) {
            return "No hearts";
        }

        StringBuilder heartsString = new StringBuilder();
        for (PlayingCard heart : hearts) {
            heartsString.append(heart.toString()).append(" ");
        }
        return heartsString.toString().trim();
    }
}
