package no.ntnu.idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * The type Deck of cards.
 * This class creates a deck of cards and shuffles it,
 * and functionality to deal cards to hand.
 *
 * @author Henrik Tobias Fredriksen
 * @version 2023-03-20
 */
public class DeckOfCards {

    private final char[] suits = {'S', 'H', 'D', 'C'};
    private ArrayList<PlayingCard> deck;

    /**
    * Instantiates a new Deck of cards.
    */
    public DeckOfCards() {
        deck = new ArrayList<>();
        for (char suit : suits) {
            for (int face = 1; face <= 13; face++) {
                deck.add(new PlayingCard(suit, face));
            }
        }
    }

    /**
     * Gets deck.
     * @return the deck
     */
    public ArrayList<PlayingCard> getDeck() {
        return deck;
    }

    /**
     * Sets deck.
     * @param deck the deck
     */
    public void setDeck(ArrayList<PlayingCard> deck) {
        this.deck = deck;
    }

    /**
     * Gets suits.
     * @return the suits
     */
    public char[] getSuits() {
        return suits;
    }

    /**
     * Shuffles the deck of cards.
     */
    public void shuffle() {

        Collections.shuffle(deck);
    }


    /**
     * Deals a hand of cards.
     * @param numCards the number of cards to be dealt
     * @return the hand
     */
    public ArrayList<PlayingCard> dealHand(int numCards) {
        ArrayList<PlayingCard> hand = new ArrayList<>();
        for (int i = 0; i < numCards; i++) {
            PlayingCard card = deck.remove(deck.size() - 1);
            hand.add(card);
        }
        return hand;
    }
/**
    public ArrayList<PlayingCard> dealFlush(int numCards) {
        ArrayList<PlayingCard> hand = new ArrayList<>();
        Iterator<PlayingCard> deckIterator = deck.iterator();

        int cardsAdded = 0;
        while(deckIterator.hasNext() && cardsAdded < numCards) {
            PlayingCard card = deckIterator.next();
            if(card.getSuit() == 'H') {
                hand.add(card);
                cardsAdded++;
            }
        }
        return hand;
    }
*/
}



