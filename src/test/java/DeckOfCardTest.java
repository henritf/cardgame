import no.ntnu.idatx2001.oblig3.cardgame.DeckOfCards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class DeckOfCardTest {


    private DeckOfCards deck;


    @BeforeEach
    void setUp() {
        deck = new DeckOfCards();
    }

    @Test
    void deckHas52Cards() {
        assertEquals(52, deck.getDeck().size());
    }
}