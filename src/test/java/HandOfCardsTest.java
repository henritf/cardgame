import no.ntnu.idatx2001.oblig3.cardgame.HandOfCards;
import no.ntnu.idatx2001.oblig3.cardgame.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class HandOfCardsTest {

    private HandOfCards hand;

    @BeforeEach
    void setUp() {
        hand = new HandOfCards();
    }

    @Test
    void getHandInitialSize() {
        assertEquals(0, hand.getHandSize());
    }

    @Test
    void addCard() {
        PlayingCard card = new PlayingCard('S', 1);
        hand.addCard(card);
        assertEquals(1, hand.getHandSize());
        assertTrue(hand.getHand().contains(card));
    }
}
