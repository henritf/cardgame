import no.ntnu.idatx2001.oblig3.cardgame.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class PlayingCardTest {

    char suit;
    int face;

    @BeforeEach
    void setUp() {
        suit = 'S';
        face = 1;
    }

    @Nested
    public class Constructor {

        @Test
        void constructor() {
            PlayingCard card = new PlayingCard(suit, face);
            org.junit.jupiter.api.Assertions.assertEquals(suit, card.getSuit());
            org.junit.jupiter.api.Assertions.assertEquals(face, card.getFace());
        }
    }

    @Nested
    public class Getters {

        @Test
        void getSuit() {
            PlayingCard card = new PlayingCard(suit, face);
            org.junit.jupiter.api.Assertions.assertEquals(suit, card.getSuit());
        }

        @Test
        void getFace() {
            PlayingCard card = new PlayingCard(suit, face);
            org.junit.jupiter.api.Assertions.assertEquals(face, card.getFace());
        }
    }

    @Nested
    public class GetAsString {

        @Test
        void getAsString() {
            PlayingCard card = new PlayingCard(suit, face);
            org.junit.jupiter.api.Assertions.assertEquals("S1", card.getAsString());
        }
    }


}
